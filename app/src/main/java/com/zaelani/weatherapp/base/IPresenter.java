package com.zaelani.weatherapp.base;

public interface IPresenter <T extends IView> {
    void onAttach(T view);
    void onDetach();
}

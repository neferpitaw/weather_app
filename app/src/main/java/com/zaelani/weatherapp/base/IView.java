package com.zaelani.weatherapp.base;

public interface IView {
    void onAttachView();
    void onDetachView();
}

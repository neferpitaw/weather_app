package com.zaelani.weatherapp.util;

public class UtilCons {
    public static final String APP_ID = "086317e3c4afff235e633b1a2756db2b";
    public static final String BASE_URL = "https://api.openweathermap.org/data/2.5/";

    public static final String KOTA_BOGOR_ID = "1648473";
    public static final String KOTA_BANDUNG_ID = "1650357";
    public static final String KOTA_CIREBON_ID = "1646170";
    public static final String KOTA_SUKABUMI_ID = "1626381";
}

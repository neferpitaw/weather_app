package com.zaelani.weatherapp.services;

import com.zaelani.weatherapp.main.weather.model.WeatherResult;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

public interface IOpenWeatherMap {
    @GET("weather")
    Call<WeatherResult> getWeatherByCity(@QueryMap Map<String, String> options);
}

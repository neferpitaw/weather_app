package com.zaelani.weatherapp.services;

import com.zaelani.weatherapp.util.UtilCons;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient {
    private static Retrofit instance;

    public static Retrofit getInstance() {
        if (instance == null) {
            instance = new Retrofit.Builder().
                    baseUrl(UtilCons.BASE_URL).
                    addConverterFactory(GsonConverterFactory.create()).build();
        }
        return instance;
    }
}

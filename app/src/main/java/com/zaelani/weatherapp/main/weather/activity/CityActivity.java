package com.zaelani.weatherapp.main.weather.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.zaelani.weatherapp.util.UtilCons;
import com.zaelani.weatherapp.R;

public class CityActivity extends AppCompatActivity implements View.OnClickListener{

    private String cityId;
    private CardView cardView1, cardView2, cardView3, cardView4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_city);

        cardView1 = findViewById(R.id.cardView1);
        cardView2 = findViewById(R.id.cardView2);
        cardView3 = findViewById(R.id.cardView3);
        cardView4 = findViewById(R.id.cardView4);

        cardView1.setOnClickListener(this);
        cardView2.setOnClickListener(this);
        cardView3.setOnClickListener(this);
        cardView4.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {
            case R.id.cardView1 :
                cityId = UtilCons.KOTA_BOGOR_ID;
                intent = new Intent(CityActivity.this, WeatherActivity.class);
                intent.putExtra("cityId", cityId);
                startActivity(intent);
                break;
            case R.id.cardView2 :
                cityId = UtilCons.KOTA_BANDUNG_ID;
                intent = new Intent(CityActivity.this, WeatherActivity.class);
                intent.putExtra("cityId", cityId);
                startActivity(intent);
                break;
            case R.id.cardView3 :
                cityId = UtilCons.KOTA_CIREBON_ID;
                intent = new Intent(CityActivity.this, WeatherActivity.class);
                intent.putExtra("cityId", cityId);
                startActivity(intent);
                break;
            case R.id.cardView4 :
                cityId = UtilCons.KOTA_SUKABUMI_ID;
                intent = new Intent(CityActivity.this, WeatherActivity.class);
                intent.putExtra("cityId", cityId);
                startActivity(intent);
                break;
        }
    }
}
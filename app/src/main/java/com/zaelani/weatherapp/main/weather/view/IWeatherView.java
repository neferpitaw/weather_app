package com.zaelani.weatherapp.main.weather.view;

import com.zaelani.weatherapp.base.IView;

public interface IWeatherView extends IView {
    void showProgressBar();
    void hideProgressBar();
}

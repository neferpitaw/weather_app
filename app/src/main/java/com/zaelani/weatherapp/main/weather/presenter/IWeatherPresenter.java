package com.zaelani.weatherapp.main.weather.presenter;

import com.zaelani.weatherapp.services.IOpenWeatherMap;

import java.util.Map;

public interface IWeatherPresenter {
    void getWeatherInfo(Map<String, String> data, IOpenWeatherMap mService);
}

package com.zaelani.weatherapp.main.weather.activity;

import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.zaelani.weatherapp.base.BaseActivity;
import com.zaelani.weatherapp.main.weather.presenter.WeatherPresenter;
import com.zaelani.weatherapp.main.weather.view.IWeatherView;
import com.zaelani.weatherapp.services.IOpenWeatherMap;
import com.zaelani.weatherapp.services.RetrofitClient;
import com.zaelani.weatherapp.util.UtilCons;
import com.zaelani.weatherapp.R;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Retrofit;

public class WeatherActivity extends BaseActivity implements IWeatherView {

    private String cityId, cityName;
    private ProgressBar progressBar;
    private WeatherPresenter weatherPresenter;

    private IOpenWeatherMap mService;

    private TextView tv_current_date,
             tv_temperature,
             tv_weather,
             tv_temp_min,
             tv_temp_max,
             tv_feels_like,
             tv_weather_desc,
             tv_wind,
             tv_humidity,
             tv_cloud;
    ImageView btn_back, icon_temp;

    @Override
    protected int getLayoutView() {
        return R.layout.activity_weather;
    }

    @Override
    protected void initComponent(Bundle saveInstanceState) {
        tv_current_date = findViewById(R.id.tv_current_date);
        tv_temperature  = findViewById(R.id.tv_temperature);
        tv_weather      = findViewById(R.id.tv_weather);
        tv_temp_min     = findViewById(R.id.tv_temp_min);
        tv_temp_max     = findViewById(R.id.tv_temp_max);
        tv_feels_like   = findViewById(R.id.tv_feels_like);
        tv_weather_desc = findViewById(R.id.tv_weather_desc);

        tv_wind         = findViewById(R.id.tv_wind);
        tv_humidity     = findViewById(R.id.tv_humidity);
        tv_cloud        = findViewById(R.id.tv_cloud);

        icon_temp       = findViewById(R.id.icon_temp);
        btn_back        = findViewById(R.id.btn_back);

        weatherPresenter = new WeatherPresenter(this, tv_current_date,
                tv_temperature,
                tv_weather,
                tv_temp_min,
                tv_temp_max,
                tv_feels_like,
                tv_weather_desc,
                tv_wind,
                tv_humidity,
                tv_cloud,
                icon_temp);

        onAttachView();
        initProgressBar();

        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        cityName = getIntent().getStringExtra("city");
        cityId = getIntent().getStringExtra("cityId");

        Map<String, String> data = new HashMap<>();
        data.put("id",      cityId);
        data.put("appid",   UtilCons.APP_ID);
        data.put("units",   "metric");

        Retrofit retrofit = RetrofitClient.getInstance();
        mService = retrofit.create(IOpenWeatherMap.class);
        weatherPresenter.getWeatherInfo(data, mService);
        hideProgressBar();
    }

    private void initProgressBar() {
        progressBar = new ProgressBar(this, null, android.R.attr.progressBarStyleSmall);
        progressBar.setIndeterminate(true);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(Resources.getSystem().getDisplayMetrics().widthPixels,
                250);
        params.addRule(RelativeLayout.CENTER_IN_PARENT);
        this.addContentView(progressBar, params);
        showProgressBar();
    }

    @Override
    public void showProgressBar() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressBar() {
        progressBar.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onAttachView() {
        weatherPresenter.onAttach(this);
    }

    @Override
    public void onDetachView() {
        weatherPresenter.onDetach();
    }
}
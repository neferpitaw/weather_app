package com.zaelani.weatherapp.main.weather.presenter;

import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.zaelani.weatherapp.base.IPresenter;
import com.zaelani.weatherapp.main.weather.view.IWeatherView;
import com.zaelani.weatherapp.main.weather.model.WeatherResult;
import com.zaelani.weatherapp.services.IOpenWeatherMap;


import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WeatherPresenter implements IPresenter<IWeatherView>, IWeatherPresenter {

    private IWeatherView iWeatherView;

    private TextView tv_current_date,
            tv_temperature,
            tv_weather,
            tv_temp_min,
            tv_temp_max,
            tv_feels_like,
            tv_weather_desc,
            tv_wind,
            tv_humidity,
            tv_cloud;

    private ImageView icon_temp;

    public WeatherPresenter(IWeatherView iWeatherView,
                            TextView tv_current_date,
                            TextView tv_temperature,
                            TextView tv_weather,
                            TextView tv_temp_min,
                            TextView tv_temp_max,
                            TextView tv_feels_like,
                            TextView tv_weather_desc,
                            TextView tv_wind,
                            TextView tv_humidity,
                            TextView tv_cloud,
                            ImageView icon_temp) {
        this.iWeatherView       = iWeatherView;
        this.tv_current_date    = tv_current_date;
        this.tv_temperature     = tv_temperature;
        this.tv_weather         = tv_weather;
        this.tv_temp_min        = tv_temp_min;
        this.tv_temp_max        = tv_temp_max;
        this.tv_feels_like      = tv_feels_like;
        this.tv_weather_desc    = tv_weather_desc;
        this.tv_wind            = tv_wind;
        this.tv_humidity        = tv_humidity;
        this.tv_cloud           = tv_cloud;
        this.icon_temp          = icon_temp;
    }

    @Override
    public void getWeatherInfo(Map<String, String> data, IOpenWeatherMap mService) {
        Call<WeatherResult> call = mService.getWeatherByCity(data);
        call.enqueue(new Callback<WeatherResult>() {
            @Override
            public void onResponse(Call<WeatherResult> call, Response<WeatherResult> response) {
                if(response.code() != 200) {
                    Log.v("RESPONSE", "results: Check the Connection.");
                } else {
                    fillWeatherResult(response);
                }
            }

            @Override
            public void onFailure(Call<WeatherResult> call, Throwable t) {

            }
        });
    }

    private void fillWeatherResult(Response<WeatherResult> response) {
        int temperature =  (int) response.body().getMain().getTemp();
        Date dateNow = new Date();
        String date = new SimpleDateFormat("dd MMMM yyyy").format(dateNow);

        String tvCurrentDate = response.body().getName() + ", " + date;
        String tvTemperature = String.valueOf(temperature);
        String tvWeather = response.body().getWeather().get(0).getMain();
        String tvTempMin = String.valueOf(response.body().getMain().getTemp_min());
        String tvTempMax = String.valueOf(response.body().getMain().getTemp_max());
        String tvFeelsLike = String.valueOf(response.body().getMain().getFeels_like());
        String tvWeatherDesc = response.body().getWeather().get(0).getDescription();

        String tvWind = String.valueOf(response.body().getWind().getSpeed());
        String tvHumidity = String.valueOf(response.body().getMain().getHumidity());
        String tvCloud = String.valueOf(response.body().getClouds().getAll());

        tv_current_date.setText(tvCurrentDate);
        tv_temperature.setText(tvTemperature);
        tv_weather.setText(tvWeather);
        tv_temp_min.setText(tvTempMin);
        tv_temp_max.setText(tvTempMax);
        tv_feels_like.setText(tvFeelsLike);
        tv_weather_desc.setText(tvWeatherDesc);

        tv_wind.setText(tvWind);
        tv_humidity.setText(tvHumidity);
        tv_cloud.setText(tvCloud);

        String icon = response.body().getWeather().get(0).getIcon();
        String iconUrl = "http://openweathermap.org/img/w/" + icon + ".png";

        Picasso.get().load(iconUrl).into(icon_temp);
    }

    @Override
    public void onAttach(IWeatherView view) {
        iWeatherView = view;
    }

    @Override
    public void onDetach() {
        iWeatherView = null;
    }
}
